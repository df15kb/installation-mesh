%title: MESH
%author: xavki

# MESH

<br>
```
                            +-------------------------+
          +-----------------+  Registry Services      +--------------+
          |                 +-------------------------+              |
    +---------------------------------+        +--------------------------------+
    |     +                           |        |                     +          |
    |   agent            service      |        |    service      ++ agent       |
    |   registry           +          |        |    +            |  registry    |
    |      +               |          |        |    |            |              |
    |      |               +          |        |    +            |              |
    |      |                          |        |                 |              |
    |      +--+ Reverse-Proxy         |        |  Reverse-Proxy  +              |
    |                 ++              |        |        ++                      |
    +---------------------------------+        +--------------------------------+
                      |                                 |
                      +---------------------------------+
                                       ^
                                       |
                                       |
                                    +-----+
                                    |     |
                                    +-----+

```

---------------------------------------------------------------------------------------

# Installation d'une application python + prometheus


```
pip install -q prometheus_client
mkdir /var/myapp/
echo "#!/usr/bin/python
from flask import Flask,request,Response
from prometheus_client import (generate_latest,CONTENT_TYPE_LATEST )
import socket
app = Flask(__name__)


@app.route('/')
def hello_world():
    hostname = socket.gethostname()
    message = 'Bonjour, je suis ' + hostname + '\n'
    return message

@app.route('/metrics')
def metrics():
    return Response(generate_latest(),mimetype=CONTENT_TYPE_LATEST)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
">/var/myapp/app.py
chmod 755 /var/myapp/app.py
```

-----------------------------------------------------------------------------


# Installation service systemd de l'application

```
echo '[Unit]
Description=MyApp service
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=root
Group=root
ExecStart=/var/myapp/app.py \
ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier=myapp

[Install]
WantedBy=multi-user.target' > /etc/systemd/system/myapp.service

systemctl enable myapp
service myapp start
```

-----------------------------------------------------------------------------

# Ajout du service à consul

```
echo '
{"service":
 {
  "name": "myapp", 
  "tags": ["myapp","python","metrics","url:myapp.localhost"], 
  "port": 80,
  "check": {
    "http": "http://localhost:80/",
    "interval": "3s"
  }
 }
}
' >/etc/consul.d/service_myapp.json
consul reload
```
