%title: MESH
%author: xavki

# MESH

<br>
```
                            +-------------------------+
          +-----------------+  Registry Services      +--------------+
          |                 +-------------------------+              |
    +---------------------------------+        +--------------------------------+
    |     +                           |        |                     +          |
    |   agent            service      |        |    service      ++ agent       |
    |   registry           +          |        |    +            |  registry    |
    |      +               |          |        |    |            |              |
    |      |               +          |        |    +            |              |
    |      |                          |        |                 |              |
    |      +--+ Reverse-Proxy         |        |  Reverse-Proxy  +              |
    |                 ++              |        |        ++                      |
    +---------------------------------+        +--------------------------------+
                      |                                 |
                      +---------------------------------+
                                       ^
                                       |
                                       |
                                    +-----+
                                    |     |
                                    +-----+

```

---------------------------------------------------------------------------------------

# Installation du reverse proxy sidecar : Linkerd


```
apt-get -y -q install openjdk-11-jdk >/dev/null
wget -q https://github.com/linkerd/linkerd/releases/download/1.6.2.2/linkerd-1.6.2.2-exec -P /usr/local/bin
chmod 755 /usr/local/bin/linkerd-1.6.2.2-exec
mkdir /etc/linkerd/
```

--------------------------------------------------------------------------------------

# configuration linkerd

```
cf fichier joint
```

```
echo '[Unit]
Description=Linkerd
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=root
Group=root
ExecStart=/usr/local/bin/linkerd-1.6.2.2-exec /etc/linkerd/linkerd.yml 
KillSignal=SIGINT
TimeoutStopSec=10
Restart=on-failure
SyslogIdentifier=linkerd

[Install]
WantedBy=multi-user.target' > /etc/systemd/system/linkerd.service

systemctl enable linkerd
service linkerd start
```
